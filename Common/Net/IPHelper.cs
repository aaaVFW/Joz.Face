﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Common.Net
{
    public class IPHelper
    {
        #region 获取IP地址
        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <param name="listProxy">代理IP列表</param>
        /// <returns>客户端IP</returns>
        public static string GetClientIP()
        {
            string listProxy = "";
            return GetClientIP(out listProxy);
        }

        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <param name="listProxy">代理IP列表</param>
        /// <returns>客户端IP</returns>
        public static string GetClientIP(out string listProxy)
        {
            return GetClientIP(HttpContext.Current, out listProxy);
        }

        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <param name="context">客户端IP</param>
        /// <param name="listProxy">代理IP列表</param>
        /// <returns></returns>
        public static string GetClientIP(HttpContext context, out string listProxy)
        {
            string hostIP = context.Request.UserHostAddress;   //代理IP地址（如果不通过代理则为客户IP地址）
            string proxyAddr = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];  //包含代理的IP地址列表
            string remoteAddr = context.Request.ServerVariables["REMOTE_ADDR"];  //客户IP地址

            listProxy = "";
            if (proxyAddr != null && proxyAddr != String.Empty)
            {
                listProxy = proxyAddr + "," + ((remoteAddr != null) ? remoteAddr : "") + "," + hostIP;
            }
            #region  这里顺序需要再考虑
            string result = hostIP;
            if (result == null || result == String.Empty)
            {
                result = remoteAddr;
            }
            if (result == null || result == String.Empty)
            {
                result = proxyAddr;
            }
            #endregion
            return result;
        }
        #endregion

        #region 远程IP解析城市
        public static bool GetAreaFromIP(string ip, ref string province, ref string city)
        {
            if (ip == "127.0.0.1" || ip == "::1") { return false; }
            return GetAreaFromSina(ip, ref province, ref city);
        }

        /// <summary>
        /// 新浪接口
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="province"></param>
        /// <param name="city"></param>
        /// <returns></returns>
        private static bool GetAreaFromSina(string ip, ref string province, ref string city)
        {
            string v = HttpRequestHelper.getContentUTF8("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=" + ip);
            try
            {
                JsonData json = JsonMapper.ToObject(v);
                province = json["province"].ToString();
                city = json["city"].ToString();
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// 淘宝接口
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="province"></param>
        /// <param name="city"></param>
        /// <returns></returns>
        private static bool GetAreaFromTaoBao(string ip, ref string province, ref string city)
        {
            string v = HttpRequestHelper.getContentUTF8("http://ip.taobao.com/service/getIpInfo.php?ip=" + ip);
            try
            {
                JsonData json = JsonMapper.ToObject(v);
                if (json["code"].ToString() == "1") { return false; }

                province = json["region"].ToString();
                city = json["city"].ToString();
                return true;
            }
            catch { return false; }
        }
        #endregion
    }
}
