﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitJson;

namespace Common.Net
{
    public class APIHelper
    {
        #region 百度API
        public static readonly string APIKEY_BAIDU = ConfigurationManager.AppSettings["ApiKey"];        
        /// <summary>
        /// 获取手机号归属地(不判断手机号是否正确)
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static string GetMobileLocation(string mobile)
        {
            string jsonResult = HttpRequestHelper.getContentUTF8("http://apis.baidu.com/apistore/mobilenumber/mobilenumber?phone=" + mobile, new Dictionary<string, string>() { { "apikey", APIKEY_BAIDU } });
            JsonData json = JsonMapper.ToObject(jsonResult);
            if (!json.Keys.Contains("retData")) return string.Empty;
            return json["retData"]["city"].ToString();
        }
        #endregion
    }
}
