﻿using System.Collections;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace Common
{
    /// <summary>
    ///TbJSon 的摘要说明
    /// </summary>
    public class JSonTb
    {
        /// <summary>   
        /// 根据Json返回DateTable,JSON数据格式如:   
        /// {table:[{column1:1,column2:2,column3:3},{column1:1,column2:2,column3:3}]}   
        /// </summary>   
        /// <param name="strJson">Json字符串</param>   
        /// <returns></returns>   
        public static DataTable JsonToDataTable(string strJson)
        {
            //取出表名   
            var rg = new Regex(@"(?<={)[^:]+(?=:\[)", RegexOptions.IgnoreCase);
            string strName = rg.Match(strJson).Value;
            DataTable tb = null;
            //去除表名   
            strJson = strJson.Substring(strJson.IndexOf("[") + 1);
            strJson = strJson.Substring(0, strJson.IndexOf("]"));

            //获取数据   
            rg = new Regex(@"(?<={)[^}]+(?=})");
            MatchCollection mc = rg.Matches(strJson);
            for (int i = 0; i < mc.Count; i++)
            {
                string strRow = mc[i].Value;
                string[] strRows = strRow.Split(',');

                //创建表   
                if (tb == null)
                {
                    tb = new DataTable();
                    tb.TableName = strName;
                    foreach (string str in strRows)
                    {
                        var dc = new DataColumn();
                        string[] strCell = str.Split(':');
                        dc.ColumnName = strCell[0].Replace("\"", "");
                        tb.Columns.Add(dc);
                    }
                    tb.AcceptChanges();
                }

                //增加内容   
                DataRow dr = tb.NewRow();
                for (int r = 0; r < strRows.Length; r++)
                {
                    dr[r] = strRows[r].Split(':')[1].Trim().Replace("，", ",").Replace("：", ":").Replace("\"", "");
                }
                tb.Rows.Add(dr);
                tb.AcceptChanges();
            }

            return tb;
        }


        /// <summary>   
        /// 根据DataTable返回JSON数据格式如:   
        /// {table:[{column1:1,column2:2,column3:3},{column1:1,column2:2,column3:3}]}   
        /// </summary>   
        /// <param name="tb">需要转换的表</param>   
        /// <returns></returns>   
        public static string DateTableToJson(DataTable tb)
        {
            if (tb == null || tb.Rows.Count == 0)
            {
                return "";
            }

            string strName = "table";
            /*  
            string strName = tb.TableName;  
            if (strName.Trim().Length == 0)  
            {  
                strName = "table";  
            }*/
            var sbJson = new StringBuilder();
            sbJson.Append("{");
            sbJson.Append("\"" + strName + "\":[");
            var htColumns = new Hashtable();
            for (int i = 0; i < tb.Columns.Count; i++)
            {
                htColumns.Add(i, tb.Columns[i].ColumnName.Trim());
            }

            for (int j = 0; j < tb.Rows.Count; j++)
            {
                if (j != 0)
                {
                    sbJson.Append(",");
                }
                sbJson.Append("{");
                for (int c = 0; c < tb.Columns.Count; c++)
                {
                    sbJson.Append(htColumns[c] + ":\"" +
                                  tb.Rows[j][c].ToString().Replace(",", "，").Replace(":", "：").Replace("\r\n", "  ") +
                                  "\",");
                }
                sbJson.Append("index:" + j); //序号   
                sbJson.Append("}");
            }
            sbJson.Append("]}");
            return sbJson.ToString();
        }
    }
}