﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Common.Unit
{
    public static class PageExtensions
    {
        /// <summary>
        /// 当前Web请求返回Json格式
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        public static void WriteJson(this object obj, object value)
        {
            ((System.Web.HttpResponse)obj).Write(JsonHelper.Serialize(value));
        }
    }
}
