﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Unit
{
    /// <summary>
    /// 通用的操作返回结果
    /// </summary>
    public class CommonCmdResult
    {
        #region 字段、属性
        bool flag = false;
        /// <summary>
        /// 与结果相关的标志或状态
        /// </summary>
        public bool Flag
        {
            get { return flag; }
            set { flag = value; }
        }
        int state = 0;
        /// <summary>
        /// 状态
        /// </summary>
        public int State
        {
            get { return state; }
            set { state = value; }
        }
        string msg = string.Empty;
        /// <summary>
        /// 消息
        /// </summary>
        public string Msg
        {
            get { return msg; }
            set { msg = value; }
        }
        object obj = new object();
        /// <summary>
        /// 相关对象
        /// </summary>
        public object Obj
        {
            get { return obj; }
            set { obj = value; }
        }
        #endregion
        #region 构造函数
        public CommonCmdResult(int state)
        {
            this.state = state;
        }
        public CommonCmdResult(bool flag)
        {
            this.flag = flag;
        }
        public CommonCmdResult(string msg)
        {
            this.msg = msg;
        }
        public CommonCmdResult(bool flag, string msg)
        {
            this.flag = flag;
            this.msg = msg;
        }
        public CommonCmdResult(int state, string msg)
        {
            this.state = state;
            this.msg = msg;
        }
        public CommonCmdResult(bool flag, int state)
        {
            this.flag = flag;
            this.state = state;
        }
        public CommonCmdResult(bool flag, int state, string msg)
        {
            this.flag = flag;
            this.state = state;
            this.msg = msg;
        }
        public CommonCmdResult(bool flag, int state, string msg, object obj)
        {
            this.flag = flag;
            this.state = state;
            this.msg = msg;
            this.obj = obj;
        }
        #endregion
    }
}
