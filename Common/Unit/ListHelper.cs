﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Unit
{
    public static class ListHelper
    {
        /// <summary>
        /// 将List转换成String
        /// </summary>
        /// <param name="list">List对象</param>
        /// <param name="seperate">分隔符</param>
        /// <returns></returns>
        public static string SeperateToString<T>(this IEnumerable<T> list, string seperate) where T : struct
        {
            string str = "";
            if (list == null || list.Count() == 0)
                return str;
            foreach (T t in list)
            {
                str += seperate + t.ToString();
            }
            return str.Substring(seperate.Length);
        }

        /// <summary>
        /// 将List转换成String
        /// </summary>
        /// <param name="list">List对象</param>
        /// <param name="seperate">分隔符</param>
        /// <param name="seperate">跳过空字符串</param>
        /// <returns></returns>
        public static string SeperateToString(this IEnumerable<string> list, string seperate, bool skipEmpty)
        {
            string str = "";
            if (list == null || list.Count() == 0)
                return str;
            foreach (String s in list)
            {
                if (s == null)
                    continue;

                if (s == String.Empty && skipEmpty)
                    continue;

                str += seperate + s.ToString();
            }

            if (str.Length > 0)
                str = str.Substring(seperate.Length);

            return str;
        }

        /// <summary>
        /// 将List转换成String
        /// </summary>
        /// <param name="list">List对象</param>
        /// <returns></returns>
        public static string SeperateToString(this IEnumerable<string> list)
        {
            return SeperateToString(list, ",", false);
        }

        /// <summary>
        /// 将List转换成String
        /// </summary>
        /// <param name="list">List对象</param>
        /// <returns></returns>
        public static string SeperateToString(this IEnumerable<int> list)
        {
            return SeperateToString(list, ",");
        }

        /// <summary>
        /// 将List转换成String
        /// </summary>
        /// <param name="list">List对象</param>
        /// <param name="seperate">分隔符</param>
        /// <returns></returns>
        public static string SeperateToString(this IEnumerable<int> list, string seperate)
        {
            string str = "";
            if (list == null || list.Count() == 0)
                return str;
            foreach (int i in list)
            {
                str += seperate + i;
            }
            return str.Substring(seperate.Length);
        }

        /// <summary>
        /// 扩展distinct 方法 调用方式lst = lst.Distinct((x, y) => x.ID==y.ID ).ToList();
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="list"></param>
        /// <param name="dele"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> list, EqualsComparer<TSource> dele)
        {
            return list.Distinct(new Compare<TSource>(dele));
        }

        /// <summary>
        /// 扩展distinct 方法 调用方式 lst = lst.Distinct(p => p.ID).ToList();
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public static IEnumerable<T> Distinct<T, V>(this IEnumerable<T> source, Func<T, V> keySelector)
        {
            return source.Distinct(new CommonEqualityComparer<T, V>(keySelector));
        }

        static void test()
        {
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            Console.WriteLine(list.SeperateToString(","));

            List<string> list2 = new List<string>();
            list2.Add("1");
            list2.Add("2");
            list2.Add("3");
            Console.WriteLine(list2.SeperateToString(",", false));
        }
    }

    #region 实现Distinct IEqualityComparer 接口
    public delegate bool EqualsComparer<T>(T x, T y);
    public class Compare<T> : IEqualityComparer<T>
    {
        private EqualsComparer<T> _equalsComparer;

        public Compare(EqualsComparer<T> equalsComparer)
        {
            this._equalsComparer = equalsComparer;
        }

        public bool Equals(T x, T y)
        {
            if (null != this._equalsComparer)
                return this._equalsComparer(x, y);
            else
                return false;
        }
        public int GetHashCode(T obj)
        {
            return obj.ToString().GetHashCode();
        }
    }

    public class CommonEqualityComparer<T, V> : IEqualityComparer<T>
    {
        private Func<T, V> keySelector;

        public CommonEqualityComparer(Func<T, V> keySelector)
        {
            this.keySelector = keySelector;
        }

        public bool Equals(T x, T y)
        {
            return EqualityComparer<V>.Default.Equals(keySelector(x), keySelector(y));
        }

        public int GetHashCode(T obj)
        {
            return EqualityComparer<V>.Default.GetHashCode(keySelector(obj));
        }
    }
    #endregion
}






