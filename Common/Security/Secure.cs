using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;

using Microsoft.Win32;
using System.Security.Cryptography;

namespace Common.Security
{
    /// <summary>
    /// 安全设置类
    /// </summary>
    public class Secure
    {
        private static char[] seeds = "0123456789abcdefghijklmnopqrstuvwxyz".ToCharArray();
        public static char[] PasswordSeeds
        {
            get { return seeds; }
        }
        /// <summary>
        /// 检测密码强度
        /// -1包含非数字或非字母，
        /// 0：禁用的超弱密码，
        /// 1：弱强度密码，
        /// 2：中强度密码，
        /// 3：强密码
        /// </summary>
        /// <param name="pswd"></param>
        /// <returns></returns>
        public static int PasswordStrongTest(string pswd)
        {
            //评估密码强度的标准（不区分大小写）
            //-1格式错（正确的）
            //0禁用的密码：12345678,abcdefg,长度小于6
            //1弱密码：aaaaa,111111,bcd678,111aaaaa,
            //3强密码：包含字母和数字且长度大于8，如果字母或数字串只为两部分则前后两部分不等长或任意一部分不为连续内容
            //2中密码：
            string s = pswd.ToLower().Trim();
            if (!Regex.IsMatch(s, "^[0-9a-z]+$")) return -1;
            if (s.Length < 6 || ((string)"01234567890").Contains(s) || ((string)"09876543210").Contains(s)
                || ((string)"abcdefghijklmnopqrstuvwxyz").Contains(s) || ((string)"zyxwvutsrqponmlkjihgfedcba").Contains(s)) return 0;

            if (Regex.IsMatch(s, "^(?<a>[0-9a-z])(\\k<a>)*((?<b>[0-9a-z])(\\k<b>)*){0,1}$"))
            {
                return 1;  //1111,aaaaaa,aabbbb,3333aaaa格式
            }

            //123123,12341234,abcdabcd
            string half = pswd.Substring(0, pswd.Length / 2);
            if ((pswd.Length == 6 || pswd.Length == 8) && half == pswd.Substring(half.Length))
            {
                if ("01234567890".Contains(half) || "09876543210".Contains(half)
                || "abcdefghijklmnopqrstuvwxyz".Contains(half) || "zyxwvutsrqponmlkjihgfedcba".Contains(half)) return 1;
            }

            if ((Regex.IsMatch(s, "^[a-z]+$") || Regex.IsMatch(s, "^[0-9]+$")))
            {
                //全为数字或全为字母（前面已经排除过一些规则）
                return 2; //减低强度要求
            }

            Match m = Regex.Match(s, "^((?<a>[a-z]+)(?<b>[0-9]+))|((?<c>[a-z]+)(?<d>[0-9]+))$");
            if (m.Success)
            {
                string a = m.Groups["a"].Success ? m.Groups["a"].Value : m.Groups["c"].Success ? m.Groups["c"].Value : "";
                string b = m.Groups["b"].Success ? m.Groups["b"].Value : m.Groups["d"].Success ? m.Groups["d"].Value : "";

                if (((string)"01234567890").Contains(b) ||
                    ((string)"09876543210").Contains(b))
                {
                    if (((string)"abcdefghijklmnopqrstuvwxyz").Contains(a) ||
                        ((string)"zyxwvutsrqponmlkjihgfedcba").Contains(a))
                    {
                        if (a.Length == b.Length)  //bcd678,bcde6789,bcd987
                        {
                            return s.Length < 10 ? 1 : 2;
                        }
                        return s.Length < 8 ? 1 : 2;   //ab1234,abcde678,876abcd
                    }
                }

            }
            return s.Length < 8 ? 2 : 3;

        }

        /// <summary>
        /// MD5摘要
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static string Md5(string plainText)
        {
            //return FormsAuthentication.HashPasswordForStoringInConfigFile(plainText, "md5");
            System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(plainText));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString().ToUpper();
        }
        public static string MD5_16(string str)
        {
            return Md5(str).Substring(8, 16);
        }
        public static string MD5_32(string str)
        {
            return Md5(str);
        }

        /// <summary>
        /// SHA1 加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SHA1(string Source_String)
        {
            byte[] temp1 = Encoding.UTF8.GetBytes(Source_String);

            SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
            byte[] temp2 = sha.ComputeHash(temp1);
            sha.Clear();

            string output = BitConverter.ToString(temp2);
            return output.Replace("-", "").ToLower();
        }

        /// <summary>
        /// 计算文件的MD5校验串
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <returns></returns>
        public static string CountFilesMD5Hash(string filePath)
        {
            string strResult = "";
            string strHashData = "";

            byte[] arrbytHashValue;

            System.IO.FileStream oFileStream = null;
            System.Security.Cryptography.MD5CryptoServiceProvider oMD5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            oFileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
            arrbytHashValue = oMD5Hasher.ComputeHash(oFileStream);//计算指定Stream 对象的哈希值
            oFileStream.Close();
            //由以连字符分隔的十六进制对构成的String，其中每一对表示value 中对应的元素；例如“F-2C-4A”
            strHashData = System.BitConverter.ToString(arrbytHashValue);
            //替换-
            strHashData = strHashData.Replace("-", "");
            strResult = strHashData;

            return strResult;
        }
    }
}
