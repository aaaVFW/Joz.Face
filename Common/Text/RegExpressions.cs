﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.Text
{
    public static class RegExpressions
    {
        public static readonly string Account = "^[a-zA-Z][_0-9a-zA-Z]+$";
        public static readonly string AccountOrMobileOrEmail = "^([a-zA-Z][_0-9a-zA-Z]+)|(\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(1[358]\\d{9})$";
        public static readonly string Chinease = "^[\u0391-\uFFE5]+$";
        public static readonly string ChineaseAndNum = "^[0-9\u0391-\uFFE5]+$";
        public static readonly string Date = "^[\\d]{4}-[\\d]{1,2}-[\\d]{1,2}$";
        public static readonly string DateTime = "^[\\d]{4}-[\\d]{1,2}-[\\d]{1,2}[\\s]+[\\d]{1,2}:[\\d]{1,2}:[\\d]{1,2}$";
        public static readonly string Email = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        public static readonly string EnglishChar = "^[a-zA-Z]+$";
        public static readonly string EnglishCharAndNum = "^[a-zA-Z0-9]+$";
        public static readonly string Float = "^(-|\\+)?(0|([1-9][0-9]*))(\\.[0-9]+){0,1}$";
        public static readonly string Identity = "^[a-zA-z][_0-9a-zA-Z]+$";
        public static readonly string Int = "^(-|\\+)?(0|([1-9][0-9]*))$";
        public static readonly string Mobile = "^(1\\d{10})$";
        public static readonly string Number = "^\\d+$";
        public static readonly string Password = "^[0-9a-zA-Z]{6,}$";
        public static readonly string QQ = "^\\d{5,10}$";
        public static readonly string Telephone = "^((\\d{2,5}[\\s]?[-]?[\\s]?))?\\d{6,8}(([\\s]?[-]?[\\s]?)\\d{1,6})?$";
        public static readonly string TelephoneA_B = "^((\\d{2,5}[\\s]?[-]?[\\s]?))\\d{6,8}$";
        public static readonly string TelephoneA_B_C = "^((\\d{2,5}[\\s]?[-]?[\\s]?))\\d{6,8}(([\\s]?[-]?[\\s]?)\\d{1,6})$";
        public static readonly string TelephoneB = "^\\d{6,8}$";
        public static readonly string Text = "^[\\w\\W]*$";
        public static readonly string Time = "^[\\d]{1,2}:[\\d]{1,2}:[\\d]{1,2}$";
        public static readonly string UFloat = "^(0|([1-9][0-9]*))(\\.[0-9]+){0,1}$";
        public static readonly string UInt = "^(0|([1-9][0-9]*))$";
        //public static readonly string  Url ="^(http[s]{0,1}://)?([a-z0-9][-a-z0-9]+\\.)?[a-z0-9][-a-z0-9]+[a-z0-9](\\.[a-z]{2,4})+(/[a-z0-9\\.,-_%\\?=&]?)?$";
        public static readonly string Url = "^[^\"><]+$"; //URL比较特殊，采用特别的方式检查（参看TextBox.CheckUrl方法）
        public static readonly string UNMATCH = "^[^\\w\\W]*$";  //都不能通过
        public static readonly string IntAndDot = "^[\\d,]+$";

        /// <summary>
        /// 身份ID15位
        /// </summary>
        public static readonly string ID15 = "^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$";
        /// <summary>
        /// 身份ID18位
        /// </summary>
        public static readonly string ID18 = "^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{4}$";

        public static bool GetMatchMobile(string Phone)
        {
            bool result = false;
            string dianxin = @"^1[3578][01379]\d{8}$";
            Regex dReg = new Regex(dianxin);
            //联通手机号正则
            string liantong = @"^1[34578][01256]\d{8}$";
            Regex tReg = new Regex(liantong);
            //移动手机号正则
            string yidong = @"^(134[012345678]\d{7}|1[34578][012356789]\d{8})$";
            Regex yReg = new Regex(yidong);

            if (dReg.IsMatch(Phone) || tReg.IsMatch(Phone) || yReg.IsMatch(Phone))
            {
                result = true;
            }
            return result;
        }

        static Dictionary<string, Regex> regDct = new Dictionary<string, Regex>();
        public static Regex GetReg(string pattern)
        {
            if (!regDct.ContainsKey(pattern)) regDct.Add(pattern, new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Multiline));
            return regDct[pattern];
        }
        /// <summary>
        /// 获取第一个匹配，并将匹配的分组内容保存到Hashtable
        /// </summary>
        /// <param name="hs">保存匹配结果</param>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <param name="groupNames">需要获取的匹配结果分组</param>
        /// <returns></returns>
        public static bool GetMatch(Hashtable hs, string input, string pattern, params string[] groupNames)
        {
            Match m = GetReg(pattern).Match(input);
            if (m.Success)
            {
                foreach (string ky in groupNames)
                {
                    if (m.Groups[ky] != null && m.Groups[ky].Success && !hs.ContainsKey(ky))
                    {
                        hs.Add(ky, m.Groups[ky].Value);
                    }
                }
            }
            return m.Success;
        }
        /// <summary>
        /// 获取全部匹配，并将每个结果的所有分组内容作为列表的一个项保存
        /// </summary>
        /// <param name="list"></param>
        /// <param name="pattern"></param>
        /// <param name="input"></param>
        /// <param name="groupNames"></param>
        public static void GetMatchList(List<Hashtable> list, string pattern, string input, params string[] groupNames)
        {
            foreach (Match m in GetReg(pattern).Matches(input))
            {
                if (m.Success)
                {
                    Hashtable hash = new Hashtable();
                    foreach (string ky in groupNames)
                    {
                        if (m.Groups[ky] != null && m.Groups[ky].Success && !hash.ContainsKey(ky))
                        {
                            hash.Add(ky, m.Groups[ky].Value);
                        }
                    }
                    list.Add(hash);
                }
            }
        }

        public static bool IsMatch(string input, string pattern)
        {
            return GetReg(pattern).IsMatch(input);
        }
        public static Match GetMatch(string input, string pattern)
        {
            return GetReg(pattern).Match(input);
        }
    }
}
