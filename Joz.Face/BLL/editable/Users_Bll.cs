﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Face.Model;
using Face.DAL;
namespace Face.BLL
{
    public partial class Users_Bll
    {
        public bool DeleteUser(int Id)
        {
            return dal.Delete(Id) > 0;
        }
    }
}

