﻿using Common.Net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Joz.Web.Models
{
    public class FaceHelper
    {
        //face++ api_key
        static string api_key = ConfigurationManager.AppSettings["FaceApiKey"];
        //face++ api_secret
        static string api_secret = ConfigurationManager.AppSettings["FaceApiSecret"];

        /// <summary>
        /// 使用face++人脸检测接口检查图片并获得图片的人脸信息
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string FacePlusPlusGetFaceInfo(byte[] bytes, string fileName, string fileExtension)
        {
            try
            {
                string content = string.Empty;
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("https://api-cn.faceplusplus.com/facepp/v3/detect");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36";
                myRequest.Method = "POST";
                myRequest.ContentLength = 0;

                Dictionary<string, string> datas = new Dictionary<string, string>();
                datas.Add("api_key", api_key);
                datas.Add("api_secret", api_secret);
                datas.Add("return_landmark", "2");
                //带有特殊的请求头及参数
                var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");
                myRequest.Method = "POST";
                byte[] byte1 = GetPayload(boundary, Encoding.GetEncoding("utf-8"), datas, bytes, fileName, fileExtension);
                myRequest.ContentLength = byte1.Length;
                myRequest.ContentType = "multipart/form-data; boundary=" + boundary;
                using (Stream newStream = myRequest.GetRequestStream())
                {
                    newStream.Write(byte1, 0, byte1.Length);
                    newStream.Close();
                }
                content = HttpRequestHelper.getContent(myRequest, Encoding.GetEncoding("utf-8"));
                return content;
            }
            catch (Exception ee)
            {
                return "{ \"time_used\": 3,	\"error_message\": \"获取face信息失败\"}";
            }
        }

        /// <summary>
        /// 使用Form形式Post的数据
        /// </summary>
        /// <param name="boundarySpliter">一串分割符号</param>
        /// <param name="kvs"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        static byte[] GetPayload(string boundary, Encoding encoding, Dictionary<string, string> kvs, byte[] bytes, string fileName, string fileExtension)
        {
            var memStream = new MemoryStream();

            // 边界符  
            //var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");
            var beginBoundary = Encoding.ASCII.GetBytes("--" + boundary);
            var endBoundary = Encoding.ASCII.GetBytes("--" + boundary + "--\r\n");
            var br = Encoding.ASCII.GetBytes("\r\n");

            #region 加入文件
            const string filePartHeader =
                "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
                 "Content-Type: application/octet-stream\r\n\r\n";

            memStream.Write(beginBoundary, 0, beginBoundary.Length);
            memStream.Write(br, 0, br.Length);
            // 文件参数头  
            var fileHeader = string.Format(filePartHeader, "image_file", Common.Security.Secure.MD5_32(fileName) + fileExtension);
            var fileHeaderBytes = encoding.GetBytes(fileHeader);
            memStream.Write(fileHeaderBytes, 0, fileHeaderBytes.Length);
            memStream.Write(bytes, 0, bytes.Length);
            memStream.Write(br, 0, br.Length);
            #endregion

            // Key-Value数据  
            var stringKeyHeader = "Content-Disposition: form-data; name=\"{0}\"" +
                                   "\r\n\r\n{1}";
            foreach (byte[] formitembytes in from string key in kvs.Keys
                                             select string.Format(stringKeyHeader, key, kvs[key])
                                                 into formitem
                                             select encoding.GetBytes(formitem))
            {
                memStream.Write(beginBoundary, 0, beginBoundary.Length);
                memStream.Write(br, 0, br.Length);
                memStream.Write(formitembytes, 0, formitembytes.Length);
                memStream.Write(br, 0, br.Length);
            }

            // 写入最后的结束边界符  
            memStream.Write(endBoundary, 0, endBoundary.Length);

            //到tempBuffer
            memStream.Position = 0;
            var tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            return tempBuffer;
        }

        /// <summary>
        /// 传入一个人脸，在集合中识别是否有相似度的人脸
        /// </summary>
        /// <param name="face_token">人脸的face_token</param>
        /// <param name="faceset_token">人脸的集合</param>
        public static string SearchFaceSetInfo(string face_token, string faceset_token)
        {
            try
            {
                string content = string.Empty;
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("https://api-cn.faceplusplus.com/facepp/v3/search");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36";
                myRequest.Method = "POST";
                myRequest.ContentLength = 0;

                Dictionary<string, string> datas = new Dictionary<string, string>();
                datas.Add("api_key", api_key);
                datas.Add("api_secret", api_secret);
                datas.Add("face_token", face_token);
                datas.Add("faceset_token", faceset_token);
                //带有特殊的请求头及参数
                var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");
                myRequest.Method = "POST";
                byte[] byte1 = GetParms(boundary, Encoding.GetEncoding("utf-8"), datas);
                myRequest.ContentLength = byte1.Length;
                myRequest.ContentType = "multipart/form-data; boundary=" + boundary;
                using (Stream newStream = myRequest.GetRequestStream())
                {
                    newStream.Write(byte1, 0, byte1.Length);
                    newStream.Close();
                }
                content = HttpRequestHelper.getContent(myRequest, Encoding.GetEncoding("utf-8"));
                return content;
            }
            catch (Exception ee)
            {
                return "{ \"time_used\": 3,	\"error_message\": \"获取face信息失败\"}";
            }
        }

        static byte[] GetParms(string boundary, Encoding encoding, Dictionary<string, string> kvs)
        {
            var memStream = new MemoryStream();

            // 边界符  
            //var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");
            var beginBoundary = Encoding.ASCII.GetBytes("--" + boundary);
            var endBoundary = Encoding.ASCII.GetBytes("--" + boundary + "--\r\n");
            var br = Encoding.ASCII.GetBytes("\r\n");

            #region 加入文件

            memStream.Write(beginBoundary, 0, beginBoundary.Length);
            memStream.Write(br, 0, br.Length);
            #endregion

            // Key-Value数据  
            var stringKeyHeader = "Content-Disposition: form-data; name=\"{0}\"" +
                                   "\r\n\r\n{1}";
            foreach (byte[] formitembytes in from string key in kvs.Keys
                                             select string.Format(stringKeyHeader, key, kvs[key])
                                                 into formitem
                                             select encoding.GetBytes(formitem))
            {
                memStream.Write(beginBoundary, 0, beginBoundary.Length);
                memStream.Write(br, 0, br.Length);
                memStream.Write(formitembytes, 0, formitembytes.Length);
                memStream.Write(br, 0, br.Length);
            }

            // 写入最后的结束边界符  
            memStream.Write(endBoundary, 0, endBoundary.Length);

            //到tempBuffer
            memStream.Position = 0;
            var tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            return tempBuffer;
        }



    }
}