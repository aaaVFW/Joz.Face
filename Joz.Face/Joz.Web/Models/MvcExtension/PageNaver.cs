﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using System.Text.RegularExpressions;

namespace Joz.Web.MvcExtension
{
    /// <summary>
    /// 分页配置
    /// </summary>
    class PagerConfig
    {
        string _PageIDKey = "pid";
        /// <summary>
        /// 分页标记(url参数)
        /// </summary>
        public string PageIDKey
        {
            get
            {
                return _PageIDKey;
            }
            set
            {
                if (value != null)
                    _PageIDKey = value;
            }
        }
        int _PageSize = 20;
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int PageSize
        {
            get
            {
                return _PageSize;
            }
            set
            {
                if (value > 0 && value <= 50001)
                    _PageSize = value;
            }
        }
        int _PageIndex = -1;
        /// <summary>
        /// 当前页编号
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (_PageIndex == -1)
                {
                    string pageid = HttpContext.Current.Request[PageIDKey] + "";
                    if (pageid == "")
                        pageid = "1";
                    if (!int.TryParse(pageid, out _PageIndex))
                    {
                        _PageIndex = 1;
                    }
                    if (_PageIndex > PageCount) _PageIndex = 1;
                }
                return _PageIndex;
            }
            set
            {
                _PageIndex = value;
            }
        }

        int _Records = 0;
        /// <summary>
        /// 总记录数
        /// </summary>
        public int Records
        {
            get
            {
                return _Records;
            }
            set
            {
                this._Records = value; // == 0 ? 1 : rowCount;
            }
        }

        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount
        {
            get
            {
                return Math.Max(1, (this.Records + this.PageSize - 1) / this.PageSize);
            }
        }

        HtmlHelper _htmlHelper;
        HtmlHelper HtmlHelper
        {
            get
            {
                return _htmlHelper;
            }
            set
            {
                _htmlHelper = value;
                Url = new UrlHelper(value.ViewContext.RequestContext);
            }
        }


        UrlHelper Url { get; set; }

        /// <summary>
        /// 获取某一页的Url路径
        /// </summary>
        /// <param name="pageID"></param>
        /// <returns></returns>
        public virtual string GetPageUrl(int pageID)
        {
            return Url.ActionSearch(new { pid = pageID });
        }
        public PagerConfig(HtmlHelper htmlHelper)
        {
            this.HtmlHelper = htmlHelper;
        }

    }
    public static class PageNaver
    {
        public static MvcHtmlString Pager(this HtmlHelper Html, int Records)
        {
            return Html.Pager(new PagerConfig(Html) { Records = Records });
        }
        public static MvcHtmlString Pager(this HtmlHelper Html, int pgSize, int Records)
        {
            return Html.Pager(new PagerConfig(Html) { PageSize = pgSize,Records = Records });
        }
        static MvcHtmlString Pager(this System.Web.Mvc.HtmlHelper Html, PagerConfig config)
        {
            StringBuilder builder = new StringBuilder();
           
            if (config.PageCount > 1)
            {
                builder.Append("<nav><ul class=\"pagination\">");
                builder.AppendFormat("<li class=\"{0}\"><a href=\"{1}\">上一页</a></li>", config.PageIndex == 1 ? "disabled" : "", config.PageIndex == 1 ? "javascript:void(0);" : config.GetPageUrl(config.PageIndex - 1));
                int minCount = 10;
                int start = 1;// 
                int end = config.PageCount;// 

                if (config.PageCount > minCount)
                {
                    start = config.PageIndex - 5 > 0 ? config.PageIndex - 5 : 1;
                    end = config.PageIndex + 4 > config.PageCount ? config.PageCount : config.PageIndex + 4;
                    if (end - start != minCount)
                    {
                        if (start == 1) end = minCount - start + 1;
                        else start = end - minCount + 1;
                    }
                }


                for (; start <= end; start++)
                {
                    if (start == config.PageIndex)
                    {
                        builder.AppendFormat("<li class=\"active\"><a href=\"javascript:void(0)\">{0}</a></li>", start.ToString());
                    }
                    else
                    {
                        builder.AppendFormat("<li><a href=\"{0}\">{1}</a></li>", config.GetPageUrl(start), start.ToString());
                    }
                }
                builder.AppendFormat("<li class=\"{0}\"><a href=\"{1}\">下一页</a></li>", config.PageIndex == config.PageCount ? "disabled" : "", config.PageIndex == config.PageCount ? "javascript:void(0);" : config.GetPageUrl(config.PageIndex + 1));
                builder.Append("</ul></nav>");
            }
            else
            {
                if (config.Records > 0)
                {
                    builder.Append("<nav><ul class=\"pagination\">");
                    builder.AppendFormat("<li class=\"{0}\"><a href=\"{1}\">上一页</a></li>", config.PageIndex == 1 ? "disabled" : "", config.PageIndex == 1 ? "javascript:void(0);" : config.GetPageUrl(config.PageIndex - 1));
                    builder.Append("<li class=\"active\"><a href=\"javascript:void(0)\">1</a></li>");
                    builder.AppendFormat("<li class=\"{0}\"><a href=\"{1}\">下一页</a></li>", config.PageIndex == config.PageCount ? "disabled" : "", config.PageIndex == config.PageCount ? "javascript:void(0);" : config.GetPageUrl(config.PageIndex + 1));
                    builder.Append("</ul></nav>");
                }
                //else
                //{
                //    builder.Append("<div style=\"width:100%;height:200px;position:absolute;top:300px;\">");
                //    builder.Append("<img src=\"../images/img-norecords.png\" style=\"position:absolute;bottom:30px;left:44%;\"/>");
                //    builder.Append("<p style=\"bottom:0px;position:absolute;text-align:center;left:48.7%;\">暂无相关数据");
                //    builder.Append("</p></div>");
                //}
            }
            
            return MvcHtmlString.Create(builder.ToString());
        }
    }
}
