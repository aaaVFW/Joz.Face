﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Joz.Web.MvcExtension
{
    public static class HtmlExtension
    {
        
        public static MvcHtmlString ContentInfoTitle(this System.Web.Mvc.HtmlHelper Html,string title)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<span class=\"glyphicon glyphicon-th content-info-title\"></span>");
            builder.Append($"<h1 style=\"display:inline\">{title}</h1>");
            return MvcHtmlString.Create(builder.ToString());
        }


        #region 视图递归
        private static Action<T> Fix<T>(Func<Action<T>, Action<T>> f)
        {
            return x => f(Fix(f))(x);
        }

        public static void Render<T>(this HtmlHelper helper, T model, Func<Action<T>, Action<T>> f)
        {
            Fix(f)(model);
        }


        #endregion






    }
}
