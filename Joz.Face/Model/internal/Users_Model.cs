﻿
    using System;
    namespace Face.Model
    {
    /// <summary>
    /// 类Users_Model
    /// </summary>
    [Serializable]
    public partial class Users_Model
    {
    /// <summary>
    /// 
    /// </summary>
    private int? id;
    private string userName;
    private string userToken;
    private string userImg;
    private DateTime? addTime;
  public Users_Model(){}
  /// <summary>
  /// 带参数的构造函数
  /// </summary>
    /// <param name="id"></param>
    /// <param name="userName"></param>
    /// <param name="userToken"></param>
    /// <param name="userImg"></param>
    /// <param name="addTime"></param>
  public Users_Model(int? id,string userName,string userToken,string userImg,DateTime? addTime)
  {
    this.id=id;
    this.userName=userName;
    this.userToken=userToken;
    this.userImg=userImg;
    this.addTime=addTime;
  }
  /// <summary>
  /// 带参数的构造函数
  ///（只包含数据表对应的属性，对于扩展的属性，应该在此方法或CopySelf方法的基础上扩展一个方法）
  /// </summary>
    /// <param name="m">要浅复制的对象</param>
  public Users_Model(Users_Model m)
  {
    this.id=m.id;
    this.userName=m.userName;
    this.userToken=m.userToken;
    this.userImg=m.userImg;
    this.addTime=m.addTime;
  }
  /// <summary>
  /// 返回一个当前对象的一个拷贝（只包含数据表对应的属性，对于扩展的属性，应该在此方法的基础上扩展一个方法）
  /// </summary>
  public Users_Model CopySelf()
  {
    return new Users_Model(this);
  }
    /// <summary>
    /// 
    /// </summary>
    public int? Id
    {
        get { return this.id; }
        set { this.id=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public string UserName
    {
        get { return this.userName; }
        set { this.userName=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public string UserToken
    {
        get { return this.userToken; }
        set { this.userToken=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public string UserImg
    {
        get { return this.userImg; }
        set { this.userImg=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public DateTime? AddTime
    {
        get { return this.addTime; }
        set { this.addTime=value; }
    }
    }
    }
  
