﻿
    using System;
    namespace Face.Model
    {
    /// <summary>
    /// 类FaceSet_Model
    /// </summary>
    [Serializable]
    public partial class FaceSet_Model
    {
    /// <summary>
    /// 
    /// </summary>
    private int? id;
    private string faceSetName;
    private string faceSetToken;
  public FaceSet_Model(){}
  /// <summary>
  /// 带参数的构造函数
  /// </summary>
    /// <param name="id"></param>
    /// <param name="faceSetName"></param>
    /// <param name="faceSetToken"></param>
  public FaceSet_Model(int? id,string faceSetName,string faceSetToken)
  {
    this.id=id;
    this.faceSetName=faceSetName;
    this.faceSetToken=faceSetToken;
  }
  /// <summary>
  /// 带参数的构造函数
  ///（只包含数据表对应的属性，对于扩展的属性，应该在此方法或CopySelf方法的基础上扩展一个方法）
  /// </summary>
    /// <param name="m">要浅复制的对象</param>
  public FaceSet_Model(FaceSet_Model m)
  {
    this.id=m.id;
    this.faceSetName=m.faceSetName;
    this.faceSetToken=m.faceSetToken;
  }
  /// <summary>
  /// 返回一个当前对象的一个拷贝（只包含数据表对应的属性，对于扩展的属性，应该在此方法的基础上扩展一个方法）
  /// </summary>
  public FaceSet_Model CopySelf()
  {
    return new FaceSet_Model(this);
  }
    /// <summary>
    /// 
    /// </summary>
    public int? Id
    {
        get { return this.id; }
        set { this.id=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public string FaceSetName
    {
        get { return this.faceSetName; }
        set { this.faceSetName=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public string FaceSetToken
    {
        get { return this.faceSetToken; }
        set { this.faceSetToken=value; }
    }
    }
    }
  
