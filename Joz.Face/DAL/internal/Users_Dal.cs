﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Common.DBUtility;
using Face.Model;
namespace Face.DAL
{
    /// <summary>
    /// 类Users_Dal
    /// 此文件由CoderGoCodeBuilder v1.0自动生成，请不要手动修改此文件，以便日后自动覆盖
    /// 2019-04-08 11:29:59
    /// </summary>
    [Serializable]
    public partial class Users_Dal : SQL_DAL_Base
    {
    private const string SQL_INSERT_USERS = @" 
  (UserName,UserToken,UserImg,AddTime)
    VALUES (@UserName,@UserToken,@UserImg,@AddTime)";
      private const string PARM_ID = "@Id";
      private const string PARM_USER_NAME = "@UserName";
      private const string PARM_USER_TOKEN = "@UserToken";
      private const string PARM_USER_IMG = "@UserImg";
      private const string PARM_ADD_TIME = "@AddTime";
    public static readonly Users_Dal Instance;
    static Users_Dal()
    {
        Instance = new Users_Dal();
        Instance.InstanceInited();
    }
    private Users_Dal()
    {
        tableOrViewName = "Users";
        primeryKeyNames = "id"; 
    }
    /// <summary>
    /// 将model实体以一条数据记录插入数据表
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要以记录插入数据库的对象</param>
    /// <returns>返回自增的Id的新增值</returns>
    public int Add(Users_Model model)
    {
        return Add(model,null);
    }
    /// <summary>
    /// 将model实体以一条数据记录插入数据表, 使用事务
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要以记录插入数据库的对象</param>
    /// <param name="model">使用的事务对象,可为null </param>
    /// <returns>Id</returns>
    public int Add(Users_Model model, SqlTransaction trans)
    {
    SqlParameter[] parameters = GetUsersParameters();
      if(model.Id == null) parameters[0].Value=0; //有可能出现与业务逻辑有关的问题
      else parameters[0].Value = model.Id;
      if(model.UserName == null) parameters[1].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[1].Value = model.UserName;
      if(model.UserToken == null) parameters[2].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[2].Value = model.UserToken;
      if(model.UserImg == null) parameters[3].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[3].Value = model.UserImg;
      if(model.AddTime == null) parameters[4].Value=DateTime.Now; //有可能出现与业务逻辑有关的问题
      else parameters[4].Value = model.AddTime;
        if(trans==null)
        {
            return int.Parse(SqlHelperWebDAL.ExecuteScalar(CommandType.Text,"INSERT INTO " + tableOrViewName + " " + SQL_INSERT_USERS+";select @@IDENTITY", parameters).ToString());
        }
        else
        {
            return int.Parse(SqlHelperWebDAL.ExecuteScalar(trans, CommandType.Text,"INSERT INTO " + tableOrViewName + " " + SQL_INSERT_USERS+";select @@IDENTITY", parameters).ToString());
        }
    }
    /// <summary>
    /// 根据model更新该实体对应的数据表记录的所有字段，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要更新的实体对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(Users_Model model)
    {
         return Update("UserName=@UserName,UserToken=@UserToken,UserImg=@UserImg,AddTime=@AddTime", model);
    }
    /// <summary>
    /// 使用事务，根据model更新该实体对应的数据表记录的所有字段，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(SqlTransaction trans, Users_Model model)
    {
         return Update(trans,"UserName=@UserName,UserToken=@UserToken,UserImg=@UserImg,AddTime=@AddTime", model);
    }
    /// <summary>
    /// 根据model更新该实体对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="UpdateSql">指定更新SQL(不含set)</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(string UpdateSql, Users_Model model)
    {
        return Update(null,UpdateSql,model);
    }
    /// <summary>
    /// 使用事务，根据model更新该实体对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <param name="UpdateSql">指定更新SQL(不含set)</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(SqlTransaction trans, string UpdateSql, Users_Model model)
    {
    if(UpdateSql.Trim()=="") throw new Exception("请指定更新细节");
    SqlParameter[] parameters = GetUsersParameters();
      if(model.Id == null) parameters[0].Value=0; //有可能出现与业务逻辑有关的问题
      else parameters[0].Value = model.Id;
      if(model.UserName == null) parameters[1].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[1].Value = model.UserName;
      if(model.UserToken == null) parameters[2].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[2].Value = model.UserToken;
      if(model.UserImg == null) parameters[3].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[3].Value = model.UserImg;
      if(model.AddTime == null) parameters[4].Value=DateTime.Now; //有可能出现与业务逻辑有关的问题
      else parameters[4].Value = model.AddTime;
    if(trans==null) return SqlHelperWebDAL.ExecuteNonQuery(CommandType.Text, "update " + tableOrViewName + " set " + UpdateSql + " where Id=@Id", parameters);
    else return SqlHelperWebDAL.ExecuteNonQuery(trans, CommandType.Text, "update " + tableOrViewName + " set " + UpdateSql + " where Id=@Id", parameters);
    }
    /// <summary>
    /// 根据主键删除该实体对应的数据表记录，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <returns>受影响的行数</returns>
    public int Delete(int id)
    {
         return Delete(null,id);
    }
    /// <summary>
    /// 使用事务，根据主键删除该实体对应的数据表记录，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <returns>受影响的行数</returns>
    public int Delete(SqlTransaction trans, int id)
    {
    if(trans==null) return SqlHelperWebDAL.ExecuteNonQuery(CommandType.Text,"delete " + tableOrViewName + " where Id=@Id",new SqlParameter("@Id",id));
	else return SqlHelperWebDAL.ExecuteNonQuery(trans,CommandType.Text,"delete " + tableOrViewName + " where Id=@Id",new SqlParameter("@Id",id));
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public Users_Model GetModelByID(int id)
    {
    return GetModel("Id=@Id", new SqlParameter("@Id",id)
    );
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public Users_Model GetModelByID(int id,SqlTransaction trans)
    {
        return GetModel("Id=@Id", trans, new SqlParameter("@Id",id)
              );
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public Users_Model GetModelByID(int id,string selectFeilds,SqlTransaction trans)
    {
        return GetModel("Id=@Id",selectFeilds ,trans, new SqlParameter("@Id",id)
        );
    }
    /// <summary>
    /// 将记录转换成实体对象
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    public Users_Model GetModel(SqlDataReader dr)
    {
        Users_Model model = new Users_Model();
        for (int i = 0; i < dr.FieldCount; i++)
    {
          switch(dr.GetName(i).ToLower())
          {
                case "id":
                    model.Id=SqlHelper.GetInt(dr["Id"]);
                    break;
                case "username":
                    model.UserName=SqlHelper.GetString(dr["UserName"]);
                    break;
                case "usertoken":
                    model.UserToken=SqlHelper.GetString(dr["UserToken"]);
                    break;
                case "userimg":
                    model.UserImg=SqlHelper.GetString(dr["UserImg"]);
                    break;
                case "addtime":
                    model.AddTime=SqlHelper.GetDateTime(dr["AddTime"]);
                    break;
                default:break;
           }
       }
       return model;
    }
    /// <summary>
    /// 将记录转换成实体对象
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    public Users_Model GetModel(DataRow dr)
    {
          Users_Model model = new Users_Model();
        for (int i = 0; i < dr.Table.Columns.Count; i++)
        {
          switch(dr.Table.Columns[i].ColumnName.ToLower())
          {
            case "id":
            model.Id=SqlHelper.GetInt(dr["Id"]);
            break;
            case "username":
            model.UserName=SqlHelper.GetString(dr["UserName"]);
            break;
            case "usertoken":
            model.UserToken=SqlHelper.GetString(dr["UserToken"]);
            break;
            case "userimg":
            model.UserImg=SqlHelper.GetString(dr["UserImg"]);
            break;
            case "addtime":
            model.AddTime=SqlHelper.GetDateTime(dr["AddTime"]);
            break;
          default:break;
          }
        }
        return model;
  }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public Users_Model GetModel(string conditionSql, params SqlParameter[] parms)
    {
        return GetModel(conditionSql,"*", parms);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="selectFields">返回属性（列），为空或null则返回所有属性（列）</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public Users_Model GetModel(string conditionSql, string selectFields, params SqlParameter[] parms)
    {
        Users_Model model = null;
    using (SqlDataReader dr = SqlHelperWebDAL.ExecuteReader(CommandType.Text, "select top 1 "+(string.IsNullOrEmpty(selectFields)?"*":selectFields)+" from " + tableOrViewName + " " + ((conditionSql.Trim() == "") ? "" : ("where " + conditionSql)), parms))
    {
    if (dr.Read()) model = GetModel(dr);
    }
    return model;
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="trans">事务(可为null)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public Users_Model GetModel(string conditionSql, SqlTransaction trans, params SqlParameter[] parms)
    {
        return GetModel(conditionSql,"*",trans,parms);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="selectFields">返回属性（列），为空或null则返回所有属性（列）</param>
    /// <param name="trans">事务(可为null)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public Users_Model GetModel(string conditionSql, string selectFields, SqlTransaction trans, params SqlParameter[] parms)
    {
        if (trans == null) return GetModel(conditionSql,selectFields, parms);
        Users_Model model = null;
    DataTable dt = SqlHelperWebDAL.GetDataSet(trans, CommandType.Text, "select top 1 "+(string.IsNullOrEmpty(selectFields)?"*":selectFields)+" from " + tableOrViewName + " " + ((conditionSql.Trim() == "") ? "" : ("where " + conditionSql)), parms).Tables[0];
    if (dt.Rows.Count > 0) model = GetModel(dt.Rows[0]);
    return model;
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="sql">完整的SQL</param>
    /// <param name="parms"></param>
    /// <returns></returns>
    public IList<Users_Model> GetModelList(string sql, params SqlParameter[] parms)
    {
        IList<Users_Model>
        modelList = new List<Users_Model>();
        using (SqlDataReader dr = SqlHelperWebDAL.ExecuteReader(CommandType.Text, sql, parms))
        {
        while (dr.Read()) modelList.Add(GetModel(dr));
        }
        return modelList;
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns></returns>
    public IList<Users_Model> GetModelList(string conditionSql, string selectFields, string orderBy, params SqlParameter[] parms)
    {
        return GetModelList(conditionSql, selectFields, orderBy, null, parms);
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="trans">事务(可为null)</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns></returns>
    public IList<Users_Model> GetModelList(string conditionSql, string selectFields, string orderBy, SqlTransaction trans, params SqlParameter[] parms)
    {
        IList<Users_Model>
        modelList = new List<Users_Model>();
        foreach(DataRow dr in GetList(conditionSql,selectFields,orderBy, trans,parms).Rows)
        {
            modelList.Add(GetModel(dr));
        }
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
         return GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelList(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelList(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on Users.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelList_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on Users.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex,int pageRecordOffset, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex,pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<Users_Model>
        modelList = new List<Users_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2005(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2005(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        string sql = SqlHelper.GetPageSQL_MSSQL2005(TableOrViewName, PrimeryKeyNames, conditionSql, "*", orderBy, pageSize, pageIndex, pageRecordOffset);
        return GetModelList(sql, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on Users.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on Users.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<Users_Model>
        modelList = new List<Users_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        DataTable dt = GetList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
        IList<Users_Model>
        modelList = new List<Users_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2000(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
      string sql = SqlHelper.GetPageSQL(TableOrViewName, PrimeryKeyNames, conditionSql, "*", orderBy, pageSize, pageIndex);
      return GetModelList(sql, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<Users_Model>
        modelList = new List<Users_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2000(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        string sql = SqlHelper.GetPageSQL(TableOrViewName, PrimeryKeyNames, conditionSql, "*", orderBy, pageSize, pageIndex, pageRecordOffset);
        return GetModelList(sql, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on Users.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on Users.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<Users_Model> GetModelListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex,int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<Users_Model>
        modelList = new List<Users_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// Internal function to get cached parameters
    /// </summary>
    /// <returns></returns>
    private static SqlParameter[] GetUsersParameters() {
    SqlParameter[] parms = SqlHelper.GetCachedParameters(SQL_INSERT_USERS);
    if (parms == null) {
    parms = new SqlParameter[] {
    new SqlParameter(PARM_ID , SqlDbType.Int)
    ,new SqlParameter(PARM_USER_NAME , SqlDbType.NVarChar , 30)
    ,new SqlParameter(PARM_USER_TOKEN , SqlDbType.VarChar , 50)
    ,new SqlParameter(PARM_USER_IMG , SqlDbType.VarChar , 100)
    ,new SqlParameter(PARM_ADD_TIME , SqlDbType.DateTime , 23)
    };
    SqlHelper.CacheParameters(SQL_INSERT_USERS, parms);
    }
    return parms;
    }
    }
 }
  
