using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Common.DBUtility;

namespace Face.DAL
{
    public class SqlHelperWebDAL : SqlHelper
    {
        //数据库连接字符串
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        public SqlHelperWebDAL()
        {
            
        }
        #region ExecuteNonQuery
        /// <summary>
        /// Execute a SqlCommand (that returns no resultset) against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// 示例:  
        ///  int result = ExecuteNonQuery(CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="cmdType">指定如何解释命令字符串(procedure, text, etc.)</param>
        /// <param name="cmdText">存储过程名称或SQL语句</param>
        /// <param name="commandParameters">参数组</param>
        /// <returns>受影响的记录数</returns>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            return ExecuteNonQuery(ConnectionString, cmdType, cmdText, commandParameters);
        }
        /// <summary>
        /// Execute a SqlCommand (that returns no resultset) 
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  int result = ExecuteNonQuery(cmd);
        /// </remarks>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="cmd">命令对象</param>
        /// <returns>an int representing the number of rows affected by the command</returns>
        public static int ExecuteNonQuery(SqlCommand cmd)
        {
            return ExecuteNonQuery(ConnectionString, cmd);
        }
        #endregion


        #region ExecuteScalar
        /// <summary>
        /// Execute a SqlCommand that returns a resultset against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  SqlDataReader r = ExecuteReader(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="cmdType">指定如何解释命令字符串(procedure, text, etc.)</param>
        /// <param name="cmdText">存储过程名称或SQL语句</param>
        /// <param name="commandParameters">SqlParameters参数</param>
        /// <returns>A SqlDataReader containing the results</returns>
        public static SqlDataReader ExecuteReader(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlDataReader rdr = ExecuteReader(ConnectionString, cmdType, cmdText, commandParameters);
            return rdr;
        }

        /// <summary>
        /// Execute a SqlCommand that returns the first column of the first record against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// 示例:  
        ///  Object obj = ExecuteScalar(CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="cmdType">指定如何解释命令字符串(procedure, text, etc.)</param>
        /// <param name="cmdText">存储过程名称或SQL语句</param>
        /// <param name="commandParameters">SqlParameters参数</param>
        /// <returns>An object that should be converted to the expected type using Convert.To{Type}</returns>
        public static object ExecuteScalar(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            return ExecuteScalar(ConnectionString, cmdType, cmdText, commandParameters);
        }
        #endregion


        /// <summary>
        /// 返回指定记录的DataSet对象
        /// </summary>
        /// <param name="cmdType">执行类型，存储过程或命令</param>
        /// <param name="cmdText">存储过程名称或SQL语句</param>
        /// <param name="commandParameters">参数</param>
        /// <returns></returns>
        public static DataSet GetDataSet(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            return GetDataSet(ConnectionString, cmdType, cmdText, commandParameters);
        }

        /// <summary>
        /// 返回指定记录的DataSet对象
        /// </summary>
        /// <param name="trans">事务(可为null)</param>
        /// <param name="cmdType">执行类型，存储过程或命令</param>
        /// <param name="cmdText">存储过程名称或SQL语句</param>
        /// <param name="commandParameters">参数</param>
        /// <returns></returns>
        public static DataSet GetDataSet(SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            if (trans == null) return GetDataSet(ConnectionString, cmdType, cmdText, commandParameters);
            return SqlHelper.GetDataSetWithTransaction(trans, cmdType, cmdText, commandParameters);
        }

        /// <summary>
        /// 为运行准备一个command
        /// </summary>
        /// <param name="cmd">SqlCommand对象</param>
        /// <param name="trans">SqlTransaction对象</param>
        /// <param name="cmdType">指定如何解释命令字符串(procedure, text, etc.)</param>
        /// <param name="cmdText">存储过程名称或SQL语句</param>
        /// <param name="cmdParms">SqlParameters参数</param>
        private static void PrepareCommand(SqlCommand cmd, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                PrepareCommand(cmd, conn, trans, cmdType, cmdText, cmdParms);
            }
        }

        /// <summary>
        /// 返回一个事务对象
        /// </summary>
        /// <param name="conn">事务的数据库连接，必须手动关闭</param>
        /// <returns></returns>
        public static SqlTransaction CreateSqlTransaction(out SqlConnection conn)
        {
            conn = new SqlConnection(ConnectionString);
            conn.Open();
            return conn.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        /// <summary>
        /// 返回一个事务对象
        /// </summary>
        /// <param name="level">指定事务的隔离级别</param>
        /// <param name="conn">事务的数据库连接，必须手动关闭</param>
        /// <returns></returns>
        public static SqlTransaction CreateSqlTransaction(IsolationLevel level, out SqlConnection conn)
        {
            conn = new SqlConnection(ConnectionString);
            conn.Open();
            return conn.BeginTransaction(level);
        }
    }
}
